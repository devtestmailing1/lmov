package com.example.mvvmkotlindatabinding.model.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.example.mvvmkotlindatabinding.model.ListResponse
import com.example.mvvmkotlindatabinding.model.Post
import com.example.mvvmkotlindatabinding.model.PostDao


@Database(entities = arrayOf(ListResponse::class),version = 1,exportSchema = false)
abstract class AppDatabase:RoomDatabase() {
    abstract fun postDao():PostDao
}