package com.example.mvvmkotlindatabinding.model

import androidx.room.Entity
import androidx.room.PrimaryKey


data class Post(val userID:Int, @PrimaryKey val id:Int, val title:String, val body:String)