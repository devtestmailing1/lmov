package com.example.mvvmkotlindatabinding.model

import android.location.Location
import androidx.room.*
import com.example.mvvmkotlindatabinding.model.database.Converters
import retrofit2.http.Url

@Entity
data class ListResponse (@PrimaryKey  var id:String, var description:String, @Ignore var location: Location, var imageUrl:String){
    constructor() : this("", "", Location(0.0,0.0,""), "")
   data class Location(var lat:Double, var lng:Double, var address:String)
}



