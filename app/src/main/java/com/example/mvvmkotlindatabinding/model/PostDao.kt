package com.example.mvvmkotlindatabinding.model

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface PostDao {

    @get:Query("SELECT * FROM listresponse")
    val all:List<ListResponse>

   /* @Insert
    fun insertAll(vararg post: Post)*/

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(note: List<ListResponse>)

    @Query("SELECT * FROM listresponse WHERE id=:userID")
    fun getLocationData(userID:String):List<ListResponse>

}