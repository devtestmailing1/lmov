package com.example.mvvmkotlindatabinding.model.database

import androidx.room.TypeConverter
import com.example.mvvmkotlindatabinding.model.ListResponse
import com.google.gson.Gson


public class Converters {


    @TypeConverter
    fun listToJson(value: List<ListResponse.Location>?): String {

        return Gson().toJson(value)
    }

    @TypeConverter
    fun jsonToList(value: String): List<ListResponse.Location>? {

        val objects = Gson().fromJson(value, Array<ListResponse.Location>::class.java) as Array<ListResponse.Location>
        val list = objects.toList()
        return list
    }




}