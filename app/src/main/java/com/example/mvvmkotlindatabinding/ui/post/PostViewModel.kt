package com.example.mvvmkotlindatabinding.ui.post

import android.service.autofill.OnClickAction
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.mvvmkotlindatabinding.base.BaseViewModel
import com.example.mvvmkotlindatabinding.interfaces.OnItemClick
import com.example.mvvmkotlindatabinding.model.ListResponse
import com.example.mvvmkotlindatabinding.model.Post

class PostViewModel:BaseViewModel() {
    private val postDescription = MutableLiveData<String>()
    private val imageURl = MutableLiveData<String>()
    private val userID = MutableLiveData<String>()

    fun bind(post:ListResponse){
        postDescription.value = post.description
        imageURl.value = post.imageUrl
        userID.value = post.id
    }

    fun getPostDescription():MutableLiveData<String>{
        return postDescription
    }

    fun getPostImageUrl():MutableLiveData<String>{
        return imageURl
    }

    fun getPostClick():MutableLiveData<String>{
        return userID
    }



}