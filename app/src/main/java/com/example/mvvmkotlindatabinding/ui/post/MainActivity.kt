package com.example.mvvmkotlindatabinding.ui.post

import android.os.Bundle
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mvvmkotlindatabinding.R
import com.example.mvvmkotlindatabinding.databinding.ActivityPostListBinding
import com.example.mvvmkotlindatabinding.fragment.HomeFragment
import com.example.mvvmkotlindatabinding.fragment.MapFragment
import com.example.mvvmkotlindatabinding.injection.ViewModelFactory
import com.google.android.material.snackbar.Snackbar

class MainActivity:AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction().replace(R.id.mycontainer, HomeFragment(),"Homee").addToBackStack(HomeFragment().javaClass.name).commit()
    }

    fun GoToNextFragment(id:String){
        val fragment = MapFragment()
        val bundle = Bundle()
        bundle.putString("id",id)
        fragment.arguments = bundle
        supportFragmentManager.beginTransaction().replace(R.id.mycontainer,fragment).addToBackStack(fragment.javaClass.name).commit()
    }

    override fun onBackPressed() {
        val count = supportFragmentManager.backStackEntryCount

        if (count == 0) {
            super.onBackPressed()
            //additional code
        } else {
            supportFragmentManager.popBackStack()
        }

    }
}