package com.example.mvvmkotlindatabinding.ui.post

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.mvvmkotlindatabinding.databinding.ItemPostBinding
import com.example.mvvmkotlindatabinding.interfaces.OnItemClick
import com.example.mvvmkotlindatabinding.model.ListResponse
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject


class PostListAdapter():RecyclerView.Adapter<PostListAdapter.ViewHolder>() {

    private lateinit var postList: List<ListResponse>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding:ItemPostBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context),
            com.example.mvvmkotlindatabinding.R.layout.item_post,parent,false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return if (::postList.isInitialized)postList.size else 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(postList[position])
    }

    fun updatePostList(postList:List<ListResponse>){
        this.postList = postList
        notifyDataSetChanged()
    }


    class ViewHolder(private val binding: ItemPostBinding):RecyclerView.ViewHolder(binding.root){

        private val viewModel =PostViewModel()


        fun bind(
            post: ListResponse){
            viewModel.bind(post)
            binding.viewModel = viewModel
           // binding.listItem.setOnClickListener()
        }

    }


}