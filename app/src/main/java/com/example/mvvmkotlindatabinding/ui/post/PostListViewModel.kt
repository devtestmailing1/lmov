package com.example.mvvmkotlindatabinding.ui.post

import android.view.View
import androidx.lifecycle.MutableLiveData
import com.example.mvvmkotlindatabinding.R
import com.example.mvvmkotlindatabinding.base.BaseViewModel
import com.example.mvvmkotlindatabinding.model.ListResponse
import com.example.mvvmkotlindatabinding.model.PostDao
import com.example.mvvmkotlindatabinding.network.PostApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PostListViewModel(private val postDao: PostDao):BaseViewModel() {
    @Inject
    lateinit var postApi: PostApi

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMessage:MutableLiveData<Int> = MutableLiveData()
    val errorClickListener = View.OnClickListener { loadPosts() }
    val postListAdapter:PostListAdapter = PostListAdapter()
    private lateinit var subscription:Disposable
    private var disposable:CompositeDisposable

    init {
        loadPosts()
        disposable = CompositeDisposable()
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }

    private fun loadPosts(){

        subscription = io.reactivex.Observable.fromCallable { postDao.all }
            .concatMap {
                    dbPostList -> if(dbPostList.isEmpty()) postApi.getListData(70).concatMap {
                    apiPostList ->   postDao.insertAll(apiPostList.body()!!)
                io.reactivex.Observable.just(apiPostList)
            }
            else{
                io.reactivex.Observable.just(dbPostList)
            }
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe{ onRetrievePostListStart()}
            .doOnTerminate{onRetrievePostListFinish()}
            .subscribe({result->onRetrievePostListSuccess(result as List<ListResponse>)},{onRetrievePostListError()})


    }



    private fun onRetrievePostListStart(){
        loadingVisibility.value=View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrievePostListFinish(){
        loadingVisibility.value = View.GONE

    }

    private fun onRetrievePostListSuccess(postList: List<ListResponse>){
        postListAdapter.updatePostList(postList)

    }

    private fun onRetrievePostListError(){
        errorMessage.value = R.string.post_error
    }


}