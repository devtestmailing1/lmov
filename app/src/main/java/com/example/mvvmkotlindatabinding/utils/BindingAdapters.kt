package com.example.mvvmkotlindatabinding.utils

import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.Bindable
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mvvmkotlindatabinding.ui.post.MainActivity
import com.example.mvvmkotlindatabinding.utils.extension.getParentActivity
import android.R
import com.example.mvvmkotlindatabinding.fragment.HomeFragment


@BindingAdapter("mutableVisibility")
fun setMutableVisibility(view: View, visibility: MutableLiveData<Int>?) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null && visibility != null) {
        visibility.observe(parentActivity, Observer { value ->
            view.visibility = value ?: View.VISIBLE
            Log.e("observervalue>", value.toString())
        })
    }
}

@BindingAdapter("mutableText")
fun setMutableText(view: TextView, text: MutableLiveData<String>?) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null && text != null) {
        text.observe(parentActivity, Observer { value -> view.text = value ?: "" })
    }
}

@BindingAdapter("adapter")
fun setAdapter(view: RecyclerView, adapter: RecyclerView.Adapter<*>) {
    view.adapter = adapter
}


/*@BindingAdapter("myclick")
fun myclick(view: View) {
   // view.adapter = adapter
    Log.e("clickhappen>>>","yeee")
}*/

@BindingAdapter("android:onClick")
fun setOnClick(view: LinearLayout,text: MutableLiveData<String>?){

    var valu: String? = null
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null && text != null) {
        text.observe(parentActivity, Observer { value->valu = value ?:"" })
    }

    view.setOnClickListener(View.OnClickListener { Log.e("click>>>>","ddd" + " "+valu)

       /* val frag =  (view.getParentActivity() as MainActivity). getSupportFragmentManager().findFragmentByTag("Homee") as HomeFragment

        valu?.let { it1 -> frag.clickclick(it1) }*/

        valu?.let { it1 -> (view.getParentActivity() as MainActivity).GoToNextFragment(it1) }

    })

}


@BindingAdapter("mutableImage")
fun setMutableImage(view: ImageView, text: MutableLiveData<String>?) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null && text != null) {
        text.observe(
            parentActivity,
            Observer { value -> Glide.with(parentActivity).load(value).into(view) })
    }

}