package com.example.mvvmkotlindatabinding.interfaces

interface OnItemClick {
    fun itemClick(userId:String)
}