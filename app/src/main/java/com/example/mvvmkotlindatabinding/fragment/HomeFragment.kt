package com.example.mvvmkotlindatabinding.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mvvmkotlindatabinding.R
import com.example.mvvmkotlindatabinding.databinding.HomeFragmentBinding
import com.example.mvvmkotlindatabinding.injection.ViewModelFactory
import com.example.mvvmkotlindatabinding.interfaces.OnItemClick
import com.example.mvvmkotlindatabinding.ui.post.MainActivity
import com.example.mvvmkotlindatabinding.ui.post.PostListAdapter
import com.example.mvvmkotlindatabinding.ui.post.PostListViewModel
import com.google.android.material.snackbar.Snackbar
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class HomeFragment : Fragment(){

    private lateinit var viewModel: PostListViewModel
    private var errorSnackbar: Snackbar? = null
    private lateinit var binding :HomeFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater,R.layout.home_fragment,container,false)
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        binding.postList.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL,false)
        viewModel = ViewModelProviders.of(this, ViewModelFactory(activity as MainActivity)).get(PostListViewModel::class.java)
        viewModel.errorMessage.observe(this, Observer { errorMessage-> if (errorMessage!=null) showError(errorMessage) else
            hideError()})
        binding.viewModel = viewModel

    }

    private fun showError(@StringRes errorMessage:Int){
        errorSnackbar = Snackbar.make(binding.root,errorMessage,Snackbar.LENGTH_INDEFINITE)
        errorSnackbar?.setAction(R.string.retry, viewModel.errorClickListener)
        errorSnackbar?.show()
    }

    private fun hideError(){
        errorSnackbar?.dismiss()
    }

    fun clickclick(userID:String){
        (activity as MainActivity).GoToNextFragment(userID)
    }

    /*override fun itemClick(userId:String) {
        (activity as MainActivity).GoToNextFragment(userId)
    }*/




}