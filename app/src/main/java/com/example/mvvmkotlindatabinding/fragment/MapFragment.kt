package com.example.mvvmkotlindatabinding.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.room.Room
import com.example.mvvmkotlindatabinding.R
import com.example.mvvmkotlindatabinding.databinding.HomeFragmentBinding
import com.example.mvvmkotlindatabinding.injection.ViewModelFactory
import com.example.mvvmkotlindatabinding.model.ListResponse
import com.example.mvvmkotlindatabinding.model.database.AppDatabase
import com.example.mvvmkotlindatabinding.ui.post.MainActivity
import com.example.mvvmkotlindatabinding.ui.post.PostListViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MapFragment : Fragment(), OnMapReadyCallback{

    private lateinit var viewModel: PostListViewModel
    private lateinit var userId:String
    lateinit var googleMap: GoogleMap;
    lateinit  var locationData:List<ListResponse>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.map_fragment, container, false)
        return view
    }

    override fun onResume() {
        super.onResume()
        getLocationData()

        Log.e("userid>>>",userId)
    }

    fun getLocationData(){
        userId = arguments?.get("id") as String
        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)

        viewModel = ViewModelProviders.of(this, ViewModelFactory(activity as MainActivity)).get(PostListViewModel::class.java)
        val db = Room.databaseBuilder(activity as MainActivity,AppDatabase::class.java,"posts").allowMainThreadQueries().build()
        locationData = db.postDao().getLocationData(userId)

        Log.e("sizee",locationData.toString())
    }

    override fun onMapReady(p0: GoogleMap?) {

        Log.e("latty>>", ">>>> "+locationData.get(0).location.lat + ">>>>"+
        locationData.get(0).location.lng + " d" + locationData.size+ "")

        googleMap = p0!!
        val place = LatLng(locationData?.get(0)?.location!!.lat, locationData!!.get(0).location.lng)
        googleMap.addMarker(MarkerOptions().position(place).title(locationData!!.get(0).location.address))
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(place))
    }
}