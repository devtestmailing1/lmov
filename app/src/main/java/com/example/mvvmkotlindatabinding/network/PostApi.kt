package com.example.mvvmkotlindatabinding.network

import com.example.mvvmkotlindatabinding.model.ListResponse
import com.example.mvvmkotlindatabinding.model.Post
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface PostApi {

    /*@GET("/posts")
    fun getPosts(): Observable<List<Post>>*/


    @GET("deliveries")
    fun getListData(@Query("limit") page: Int): Observable<Response<List<ListResponse>>>
}