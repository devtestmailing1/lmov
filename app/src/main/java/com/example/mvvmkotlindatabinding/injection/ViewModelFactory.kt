package com.example.mvvmkotlindatabinding.injection

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import com.example.mvvmkotlindatabinding.model.Post
import com.example.mvvmkotlindatabinding.model.database.AppDatabase
import com.example.mvvmkotlindatabinding.ui.post.PostListViewModel
import java.lang.IllegalArgumentException

class ViewModelFactory(private val activity:AppCompatActivity):ViewModelProvider.Factory {
    override fun <T:ViewModel?> create(modelClass:Class<T>):T{
        if (modelClass.isAssignableFrom(PostListViewModel::class.java)){
            val db = Room.databaseBuilder(activity.applicationContext,AppDatabase::class.java,"posts").build()
         @Suppress("UNCHECKED_CAST")
         return PostListViewModel(db.postDao()) as T
        }
        throw IllegalArgumentException("unknown ViewModel class")
    }
}